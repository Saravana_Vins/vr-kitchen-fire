﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtinguisherControle : MonoBehaviour {
    public static ExtinguisherControle SP;

    public ParticleSystem smokeEffect;
    public bool canExtinguish;
    public CapsuleCollider collider;

    bool fire;
	// Use this for initialization
	void Start () {
        SP = this;
        smokeEffect.Stop();
        collider = smokeEffect.gameObject.GetComponent<CapsuleCollider>();
        collider.enabled = false;        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.LeftShift) & canExtinguish)
        {
            collider.enabled = true;
            smokeEffect.Play();
        }

        if(Input.GetKeyUp(KeyCode.LeftShift) & canExtinguish)
        {
            collider.enabled = false;
            smokeEffect.Stop();
        }
    }
}
