﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogController : MonoBehaviour {
    public static FogController SP;
    public GameObject[] fireSources;
    public GameObject smokeSource;    
    public float fireHealth = 50;
    public float smokeHealth = 100;
    public float maxFireHealth = 50;
    public float maxSmokeHealth = 500;
    public float extinguishPower = 5;
    public float fireRegen = 5;
    public float waitForFire =5;
    public float fireInterval = 2;
    public float changeModelWaitTime = 5;
    private void Awake()
    {
        StopFire();
        StartCoroutine(StartFire());
        StopFinalSmoke();
    }
    // Use this for initialization
    void Start () {
        SP = this;
        timeCounter = fireCountInterval;
             
    }
    public bool extinuished;
    public int fireCount=0;
    public float fireCountInterval = 2;
    float timeCounter=0;
    public float finalSmokeDestroyWaitTime=20;
	// Update is called once per frame
	void Update () {              
        timeCounter += Time.deltaTime;
        while (!extinuished & timeCounter>fireCountInterval)
        {
            fireCount = 0;
            foreach (GameObject fS in fireSources)
            {
                fireCount += fS.transform.childCount;
            }
            if (fireCount <= 4)
            {
                extinuished = true;
                StartCoroutine(FinalSmokeDestroy());
            }
            timeCounter = 0;
        }        
	}

    public IEnumerator FinalSmokeDestroy()
    {
        yield return new WaitForSeconds(finalSmokeDestroyWaitTime);
        ParticleSystem[] finalSmokes = smokeSource.GetComponentsInChildren<ParticleSystem>();
        if(finalSmokes.Length!=0)
        foreach (ParticleSystem fS in finalSmokes)
        {
            fS.Stop();
            yield return new WaitForSeconds(Random.Range(2,6));
        }
    }

    public IEnumerator StartFire()
    {
        Debug.Log("StartFire");
        yield return new WaitForSeconds(waitForFire);
        Debug.Log("StartFire after 5");
        foreach (GameObject fireSource in fireSources)
        {
            //GameObject[] firebase = fireSource.transform.Get();
            ParticleSystem[] fires = fireSource.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem fire in fires)
            {
                fire.Play();
                Debug.Log(fire.name+" Started");
                yield return new WaitForSeconds(fireInterval);                
            }
        }
        StartFinalSmoke();
        yield return new WaitForSeconds(changeModelWaitTime);
        ModelChanger.SP.BurnedKitchen(true);
    }
    public void StartFinalSmoke()
    {
        ParticleSystem[] finalSmokes = smokeSource.GetComponentsInChildren<ParticleSystem>();
        if (finalSmokes.Length != 0)
            foreach (ParticleSystem fS in finalSmokes)
            {
                fS.Play();                
            }
    }
    public void StopFinalSmoke()
    {
        ParticleSystem[] finalSmokes = smokeSource.GetComponentsInChildren<ParticleSystem>();
        if (finalSmokes.Length != 0)
            foreach (ParticleSystem fS in finalSmokes)
            {
                fS.Stop();
            }
    }
    public void StopFire()
    {
        foreach (GameObject fireSource in fireSources)
        {
            ParticleSystem[] fires = fireSource.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem fire in fires)
            {
                fire.Stop();
            }
        }
        
    }
}
