﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum SmokeOrFire
{
    Fire,
    Smoke
}
public class ExtinguishFire : MonoBehaviour {
    public float fireHealth=50;
    public float maxFireHealth = 50;
    public float extinguishPower = 5;
    public float fireRegen = 5;
    public bool isOnFire = true;
    public FogController fogControl;
    public ParticleSystem ps;
    public SmokeOrFire Type;
    public List<ParticleSystem.Particle> enterPs = new List<ParticleSystem.Particle>();

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
        fogControl = GameObject.Find("FogController").GetComponent<FogController>();

        if (!fogControl)
            return;
        if (Type == SmokeOrFire.Fire)
        {
            fireHealth = fogControl.fireHealth;
            maxFireHealth = fogControl.maxFireHealth;
        }
        else if(Type==SmokeOrFire.Smoke)
        {
            fireHealth = fogControl.smokeHealth;
            maxFireHealth = fogControl.maxSmokeHealth;
        }
        
        extinguishPower = fogControl.extinguishPower;
        fireRegen = fogControl.fireRegen;
        
    }
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        
        if (isOnFire)
        {
            fireHealth += Time.deltaTime * fireRegen;
            if (fireHealth > maxFireHealth)
            {
                fireHealth = maxFireHealth;
            }
        }
		
	}

    void OnParticleTrigger()
    {
        if (!ps)
            return;
        int numberOfEnterTriggers = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter,enterPs);

        if (numberOfEnterTriggers > 0)
        {
            //Debug.Log( numberOfEnterTriggers+" Triggers.");
            Extinguishing();
        }
    }

    void Extinguishing()
    {
        fireHealth -= extinguishPower;
        if (fireHealth <= 0)
        {
            fireHealth = 0;
            isOnFire = false;
            GetComponent<ParticleSystem>().Stop();            
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.name== "FlamethrowerFire")
        {
            //Debug.Log("Extinguishing.....");
            Extinguishing();
        }
    }
}
