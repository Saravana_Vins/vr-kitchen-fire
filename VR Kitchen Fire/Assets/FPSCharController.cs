﻿using UnityEngine;
using System.Collections;

public class FPSCharController : MonoBehaviour {
    public float speed = 2f;
    public float sensitivity = 2f;
    public bool isGround;
    public float gravity;
    public float fallsSpeed;

    CharacterController player;
    float moveFB;
    float rotateLR;

    float rotX;
    float rotY;
	// Use this for initialization
	void Start () {
        player = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!Input.GetKey(KeyCode.LeftControl))
            moveFB = Input.GetAxis("Vertical") * speed;
        else
            moveFB = 0;
        rotateLR = Input.GetAxis("Horizontal")*speed;

        Vector3 movement = new Vector3(0, 0,moveFB);
        movement = transform.rotation * movement;
        player.Move(movement*Time.deltaTime);        
        transform.Rotate(0,rotateLR,0);
        //Debug.Log("FPS Grounded?="+player.isGrounded);
        IsGrounded();
        Fall();
	}
    
    
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Other trigger name=" + other.tag);
    }

    void Fall()
    {
        if (!isGround)
        {
            fallsSpeed += gravity * Time.deltaTime;
        }
        else
        {
            if(fallsSpeed>0)
            fallsSpeed = 0;
        }
        player.Move(new Vector3(0,-fallsSpeed)*Time.deltaTime);
    }
    void IsGrounded()
    {
        isGround = Physics.Raycast(transform.position,-transform.up,player.height/1.9f);
    }
}
