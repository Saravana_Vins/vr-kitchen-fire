﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetAndReplaceExtinguisher : MonoBehaviour {
    public GameObject firstPerson;
    public GameObject extinguisher;
    public Transform extinguisherHand;

    public bool canExtinguish;
    public bool canGetOrReplace;
    public Vector3 defaultPlace;
    public Vector3 defaultScale;
    public Quaternion defaltRotation;
	// Use this for initialization
	void Start () {
        defaultPlace = extinguisher.transform.position;
        defaultScale = extinguisher.transform.localScale;
        defaltRotation = extinguisher.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        if (canGetOrReplace)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (canExtinguish)
                {
                    ReplaceExtinguisher();
                }
                else
                {
                    GetExtinguisher();
                }
            }
        }
	}
    
    
    void OnGUI()
    {        
        if (canGetOrReplace)
        {
            if(!canExtinguish)
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 400, 40), "Press 'E' to Get Extinguisher.");
            else
                GUI.Label(new Rect(Screen.width/2, Screen.height/2, 400, 40), "Press 'E' to Replace Extinguisher.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "FPSCharController")
        {
            canGetOrReplace = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name == "FPSCharController")
        {
            canGetOrReplace = false;
        }
    }

    void ReplaceExtinguisher()
    {
        extinguisher.transform.parent = null;        
        extinguisher.transform.position = defaultPlace;
        extinguisher.transform.rotation = defaltRotation;
        extinguisher.transform.localScale = defaultScale;
        extinguisher.GetComponent<ExtinguisherControle>().canExtinguish = false;
        canExtinguish = false;
    }
    void GetExtinguisher()
    {
        extinguisher.transform.parent = firstPerson.transform;
        extinguisher.transform.position = extinguisherHand.position;
        extinguisher.transform.rotation = extinguisherHand.rotation;
        extinguisher.GetComponent<ExtinguisherControle>().canExtinguish = true;
        canExtinguish = true;
    }
}
