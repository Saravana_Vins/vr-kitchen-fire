﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTrigger : MonoBehaviour {
    public ParticleSystem ps;

    public List<ParticleSystem.Particle> enterPs = new List<ParticleSystem.Particle>();
    public List<ParticleSystem.Particle> exitPs = new List<ParticleSystem.Particle>();
    public List<ParticleSystem.Particle> insidePs = new List<ParticleSystem.Particle>();
    // Use this for initialization
    void Start () {
        ps = GetComponent<ParticleSystem>();
	}

    void OnParticleTrigger()
    {
        int numberOfEnterTrigger = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Inside,enterPs);
        int numberOfExitTrigger = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Exit, exitPs);
        if (numberOfEnterTrigger > 0)
        {            
            Debug.Log(numberOfEnterTrigger + " Enter Triggerd Particles.");

            for (int i = 0; i < numberOfEnterTrigger; i++)
            {
                ParticleSystem.Particle p = enterPs[i];
                p.startColor = Color.blue;
                enterPs[i] = p;
            }
            ps.SetTriggerParticles(ParticleSystemTriggerEventType.Inside, enterPs);
        }
        if (numberOfExitTrigger > 0)
        {
            Debug.Log(numberOfExitTrigger + " Exit Triggerd Particles.");
            for (int j = 0; j < numberOfExitTrigger; j++)
            {
                ParticleSystem.Particle p = exitPs[j];
                p.startColor = Color.green;
                exitPs[j] = p;
            }
            ps.SetTriggerParticles(ParticleSystemTriggerEventType.Exit, exitPs);
        }
        
        
        

        
        
    }

	// Update is called once per frame
	void Update () {
		
	}
}
