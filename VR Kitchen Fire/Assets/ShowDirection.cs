﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using VRTK;

public class ShowDirection : MonoBehaviour {
    public Transform door;
    public GameObject arrowMark;
    public GameObject arrowMark2;    
    NavMeshAgent navAgent;
    NavMeshPath navMeshPath;
    Vector3 target;
    public Vector3 camPos;
    public Transform mainCam;
    public float arrowDistance=1;
    public Vector3 locPos;
    public Vector3 pos;
    // Use this for initialization
    void Start () {
        navAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();        
        target = door.position;
        arrowMark.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("Camera Y="+transform.localPosition.y);
        if (canShowDir & !AnimateFog.SP.canExtinguish)
        {
            arrowMark2.SetActive(true);
        }
        else
        {
            arrowMark2.SetActive(false);
        }
        navAgent.CalculatePath(target,navMeshPath);
        int pathCorners = navMeshPath.corners.Length;
        //Debug.Log("Path Corners="+navMeshPath.corners.Length);
        if (pathCorners > 1)
        {
            Vector3 lookCorner = navMeshPath.corners[1];            
            lookCorner.y =arrowMark.transform.position.y;
            arrowMark.transform.LookAt(lookCorner);
        }
        arrowMark2.transform.rotation = arrowMark.transform.rotation;
        
        camPos= mainCam.transform.position;
        locPos= transform.localPosition;
        pos= transform.position;
        camPos.y = 0;
        arrowMark.transform.position = camPos;
        //Debug.Log("Trandform.forwrd="+ transform.forward);
        Vector3 frontDist = transform.forward*arrowDistance;
        arrowMark2.transform.localPosition = camPos+ frontDist; 
    }
    public bool canShowDir;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Target"))
        {
            canShowDir = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Target"))
        {
            canShowDir = false;
        }
    }
}
