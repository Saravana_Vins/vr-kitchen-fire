﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelChanger : MonoBehaviour {
    public static ModelChanger SP;
    public GameObject kitchen;
    public GameObject burnedKitchen;
	// Use this for initialization
	void Start () {
        SP = this;
        BurnedKitchen(false);
    }		

    public void BurnedKitchen(bool status)
    {
        kitchen.SetActive(!status);
        burnedKitchen.SetActive(status);
    }
}
