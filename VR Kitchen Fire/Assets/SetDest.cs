﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SetDest : MonoBehaviour {
    public Transform target;
    public GameObject arrowMark;
    public GameObject prime;
    NavMeshAgent navAgent;
    NavMeshPath navMeshPath;
    bool setZero=true;
	// Use this for initialization
	void Start () {
        prime = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        prime.transform.localScale = new Vector3(0.2f,0.2f,0.2f);
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.SetDestination(target.position);        
    }    
    Vector3 lookTarget;
    // Update is called once per frame
    void Update () {
        Debug.Log("Path Point Length="+navAgent.path.corners.Length);
        
        
        if (navAgent.path.corners.Length>1)
        {            
            lookTarget = navAgent.path.corners[1];
            prime.transform.position = lookTarget;
            lookTarget.y = arrowMark.transform.position.y;
            arrowMark.transform.LookAt(lookTarget);
        }
        
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (setZero)
            {
                navAgent.speed = 0;
                setZero = false;
            }
            else
            {
                navAgent.speed = 1.0f;
                setZero = true;
            }
        }
	}
}
