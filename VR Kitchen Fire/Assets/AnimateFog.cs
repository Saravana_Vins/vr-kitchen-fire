﻿namespace VRTK
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AnimateFog : MonoBehaviour
    {
        public static AnimateFog SP;

        public ParticleSystem smokeEffect;
        public bool canExtinguish;
        public CapsuleCollider collider;

        // Use this for initialization
        void Start()
        {
            SP = this;
            smokeEffect.Stop();
            collider = smokeEffect.gameObject.GetComponent<CapsuleCollider>();
            collider.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (GetComponent<VRTK_InteractGrab>().GetGrabbedObject() != null)
            {
                if (GetComponent<VRTK_InteractGrab>().GetGrabbedObject().CompareTag("GameController"))
                {
                    canExtinguish = true;
                    var controllerEvents = GetComponent<VRTK_ControllerEvents>();
                    //GetComponent<VRTK_Pointer>().enabled = false;
                    if (controllerEvents.triggerPressed)
                    {
                        collider.enabled = true;
                        smokeEffect.Play();
                    }
                    else
                    {
                        collider.enabled = false;
                        smokeEffect.Stop();
                    }
                }
                else
                {
                    //GetComponent<VRTK_Pointer>().enabled = true;
                }
            }
        }
    }
}